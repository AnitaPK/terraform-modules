output "vpc_net_id" {
  value = google_compute_network.custom-test.id
}

output "vpc_net_name" {
  value = google_compute_network.custom-test.name
}

output "vpc_subnet_id" {
    value = google_compute_subnetwork.sub-network.id
}

output "my_subnet_name" {
  value = google_compute_subnetwork.sub-network.name
}