resource "google_compute_network" "custom-test" {
  name = var.vpc_name
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "sub-network" {
  name = var.vpc_subnet_name
  ip_cidr_range = var.ip_cidr_range_value
  network       = var.network_id
 
}

resource "google_compute_firewall" "default" {
  name    = var.firewall_name
  network = var.firewallnetwork

  allow {
    protocol = var.protocol_icmp
  }

  allow {
    protocol = var.protocol_tcp
    ports    = ["22"]
  }

  
 allow {
    protocol = var.protocol_udp
    ports    = ["0-65535"]
 }

    priority = var.firewall_priority
    source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_instance" "vm_instance" {
  name         = var.vm_name
  machine_type = var.vm_machine_type
  zone = var.vm_zone

  boot_disk {
    initialize_params {
      image = var.vm_image
    }
  }

  network_interface {
    network = var.vpc_network
     subnetwork = var.vpcsubnet
  }
}

