variable "vm_name" {
  type = string
}

variable "vm_machine_type" {
  type = string
}

variable "vm_zone" {
  type = string
}

variable "vm_image" {
  type = string
}

variable "vpc_network" {
  type = string
}

variable "vpcsubnet" {
    type = string
}

variable "vpc_name" {
  type = string
}

variable "vpc_subnet_name" {
  type = string
}

variable "ip_cidr_range_value" {
  type = string
}

variable "network_id" {
  type = string
}

variable "firewall_name" {
  type = string
}

variable "firewallnetwork" {
  type = string
}

variable "protocol_icmp" {
  type = string
}

variable "protocol_tcp" {
  type = string
}

variable "protocol_udp" {
  type = string
}

variable "firewall_priority" {
  type = string
}